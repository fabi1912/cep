package com.itau.treinamento.cep.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.treinamento.cep.client.Cep;
import com.itau.treinamento.cep.client.CepClient;

@Service
public class CepService {
	
	@Autowired
	private CepClient cepClient;
	
	
	public Cep buscaCep(String cep) {
		return cepClient.getCep(cep);
	}

}
