package com.itau.treinamento.cep.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.treinamento.cep.client.Cep;
import com.itau.treinamento.cep.service.CepService;

@RestController
@RequestMapping("/cep")
public class CepController {
	
	@Autowired
	private CepService cepService;
	
	Logger logger = LoggerFactory.getLogger(CepController.class);

	@GetMapping("/{cep}")
	public Cep buscaCep(@PathVariable String cep) {
		
		logger.info("buscando cep " + cep);
		return cepService.buscaCep(cep);
	}

}
