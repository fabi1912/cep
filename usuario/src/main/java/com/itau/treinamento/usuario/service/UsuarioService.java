package com.itau.treinamento.usuario.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import com.itau.treinamento.usuario.client.Cep;
import com.itau.treinamento.usuario.client.CepClient;
import com.itau.treinamento.usuario.model.Usuario;
import com.itau.treinamento.usuario.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private CepClient cepClient;
	
    @NewSpan(name = "viacep-service")
	public Usuario salvaUsuario(@SpanTag Usuario usuario) {
		
		Cep cep = cepClient.buscaCep(usuario.getCep());
		usuario.setBairro(cep.getBairro());
		usuario.setLogradouro(cep.getLogradouro());
		
		return usuarioRepository.save(usuario);
	}

}
