package com.itau.treinamento.usuario.model.dto;

import com.itau.treinamento.usuario.model.Usuario;

public class UsuarioMapper {
	
	public static UsuarioResponse toUsuarioResponse (Usuario usuario) {
		UsuarioResponse usuarioResponse = new UsuarioResponse();
		usuarioResponse.setBairro(usuario.getBairro());
		usuarioResponse.setCep(usuario.getCep());
		usuarioResponse.setId(usuario.getId());
		usuarioResponse.setNome(usuario.getNome());
		usuarioResponse.setLogradouro(usuario.getLogradouro());
		return usuarioResponse;
	}
	
	public static Usuario toUsuario(UsuarioRequest usuarioRequest) {
		Usuario usuario = new Usuario();
		usuario.setCep(usuarioRequest.getCep());
		usuario.setNome(usuarioRequest.getNome());
		return usuario;
	}

}
