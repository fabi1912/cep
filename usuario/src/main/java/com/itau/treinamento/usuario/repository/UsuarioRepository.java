package com.itau.treinamento.usuario.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.treinamento.usuario.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

}
