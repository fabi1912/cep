package com.itau.treinamento.usuario.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.itau.treinamento.usuario.model.dto.UsuarioMapper;
import com.itau.treinamento.usuario.model.dto.UsuarioRequest;
import com.itau.treinamento.usuario.model.dto.UsuarioResponse;
import com.itau.treinamento.usuario.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	Logger logger = LoggerFactory.getLogger(UsuarioController.class);
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public UsuarioResponse salvaUsuario(@Valid @RequestBody UsuarioRequest usuarioRequest) {
		
		logger.info("salvando usuario " + usuarioRequest.getNome());
		
		return UsuarioMapper.toUsuarioResponse(
				usuarioService.salvaUsuario(UsuarioMapper.toUsuario(usuarioRequest)));
		
	}

}
