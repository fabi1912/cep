package com.itau.treinamento.usuario.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "cep")
public interface CepClient {
	

	@GetMapping("/cep/{cep}")
	public Cep buscaCep(@PathVariable String cep);

}
