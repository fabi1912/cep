package com.itau.treinamento.usuario.model.dto;

import javax.validation.constraints.NotBlank;

public class UsuarioRequest {
	
	@NotBlank
	private String nome;
	@NotBlank
	private String cep;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	

}
